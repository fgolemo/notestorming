'use strict';

controllerModule.controller(
    'NotesCtrl', ['$scope',
        'Articles',
        'NoteContainer',
        'NoteInterface',
        '$debounce',
        function ($scope, Articles, NoteContainer, ni, $debounce) {
//        NoteContainer.test("Yuri");
            NoteContainer.init($scope);
            $scope.noteContainer = NoteContainer;
            $scope.changeFilter = function (filterVal) {
                if (filterVal == NoteContainer.everythingLabel)
                    $scope.noteContainer.filter = '';
                else
                    $scope.noteContainer.filter = filterVal;
            };
            $scope.noteFilter = function (name) {
                $scope.noteContainer.filterType = name;
                $scope.noteContainer.filter = "";
                $scope.noteContainer.search = "";
            };
            $scope.filterEmpty = function () {
                return ($scope.noteContainer.filter == '' && $scope.noteContainer.search == '');
            };
            $scope.listFilterIsActive = function (name) {
                return (
                    $scope.noteContainer.filter == name ||
                        (name == NoteContainer.everythingLabel && $scope.noteContainer.filter == '')
                    );
            };
            $scope.listNoteIsActive = function (id) {
                return ($scope.noteContainer.activeNoteID == id);
            };
            $scope.new = {
                idea: false,
                comment: false
            };
            $scope.idea = $scope.noteContainer.ideaFactory();
            $scope.comment = $scope.noteContainer.commentFactory();
            $scope.make = function (what) {
                $scope.new[what] = true;
            };
            $scope.save = function (what) {
                $scope.new[what] = false;
                //$scope.noteContainer.notes[$scope.noteContainer.activeNoteID].addIdea(
                if (what == "idea") {
                    ni.addIdea($scope.noteContainer.activeNoteID, $scope.idea);
                    $scope.idea = $scope.noteContainer.ideaFactory();
                } else {
                    $scope.noteContainer.active.addComment(
                        $scope.comment.message,
                        0
                    );
                    $scope.comment = $scope.noteContainer.commentFactory();
                }
            };
            $scope.remove = function (what, id) {
                if (what == "idea") ni.removeIdea($scope.noteContainer.activeNoteID, id);
            }
            $scope.authorColor = function (author) {
                return $scope.noteContainer.authors[author].color;
            }
            $scope.options = {
                tags: {
                    // options for select2
                    tags: $scope.noteContainer.flat.tags
                },
                domains: {
                    tags: $scope.noteContainer.flat.domains
                }
            };
            $scope.newNote = function () {
                $scope.noteContainer.loadNote(-1);
//                console.log({list:$scope.noteContainer.notes});
            };
            $scope.removeNote = function() {
                ni.removeNote($scope.noteContainer.activeNoteID,
                    $scope.noteContainer.dataCallback,
                    $scope);
                $scope.newNote();
            }
            $scope.test = function() {
                console.log("something");
            }
            var blockUpdate = false;
            var saveNote = function (newVal, oldVal) {
                if (newVal != oldVal && newVal.name) {
                    if ($scope.noteContainer.activeNoteID == -1) {
                        blockUpdate = true;
                        var ref = ni.addNote(newVal);
                        $scope.noteContainer.activeNoteID = ref.name();
                        $scope.noteContainer.active.id = ref.name();
                        blockUpdate = false;
                    } else {
                        if (!blockUpdate)
                            ni.updateNote(newVal,$scope.noteContainer.activeNoteID);
                    }
                }
            };
            $scope.$watch('noteContainer.active', $debounce(saveNote, 1000), true);

//            $scope.articles = [{name:"hi"}];

            $scope.remoteQuery = function () {
                console.log($scope.queryKeywords);
                //Articles.query();
                var aTest = Articles.get({keywords: $scope.queryKeywords}, function(articles) {
                    console.log("query done");
                    console.log(articles);
                    $scope.articles = articles;
                });
                console.log(aTest);
            }

        }]);

