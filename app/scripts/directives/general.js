'use strict';

var app = angular.module('flagisv3App.directives', []);
//app.directive(
//    'domainswitch', function () {
//        return {
//            restrict: 'A',
//            scope: false,
//            link: function (scope, element, attrs) {
//                element.click(function () {
//                    scope.$apply(function () {
//                        scope.noteContainer.activeDomain = attrs.domainswitch
//                    });
//                });
//            }
//        };
//    });

app.directive(
    'tabbed', function () {
        return {
            restrict: 'A',
            scope: false,
            link: function (scope, element, attrs) {
                element.click(function (e) {
                    e.preventDefault()
                    $(this).tab('show')
                })

            }
        };
    });

