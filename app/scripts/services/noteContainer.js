'use strict';

angular.module('flagisv3App.factories', [])
    .factory('NoteInterface', [function NoteInterface() {
        var ref = new Firebase("https://flagisv3.firebaseio.com/notes");
        return {
            getNotes: function (dataCallback, scope, activeID, setNote) {
                var notes = [];
                ref.on("child_added", function (noteSnapshot) {
                    var note = noteSnapshot.val();
                    note.id = noteSnapshot.name();
                    notes.push(note);
                });
                ref.on("child_changed", function (noteSnapshot) {
//                    console.log("update incomming" + noteSnapshot.name() + new Date().getTime());
                    for (var noteIndex in notes) {
                        if (notes[noteIndex].id == noteSnapshot.name()) {
                            notes[noteIndex] = noteSnapshot.val();
                            if (activeID() == noteSnapshot.name())
                                setNote(noteSnapshot.val());
                            break;
                        }
                    }
                    dataCallback();
                    if (!scope.$$phase) scope.$apply();
                });
                ref.on("child_removed", function (noteSnapshot) {
                    for (var noteIndex in notes) {
                        if (notes[noteIndex].id == noteSnapshot.name()) {
                            notes.splice(noteIndex,1);
                            break;
                        }
                    }
                    dataCallback();
                    if (!scope.$$phase) scope.$apply();
                });
                ref.once('value', function () {
                    dataCallback();
                    if (!scope.$$phase) scope.$apply(); //force reload
                });
                return notes;
            },
            addNote: function (note) {
                var newRef =  ref.push(note);
                ref.child(newRef.name()).update({id:newRef.name()});
                return newRef;
            },
            removeNote: function (id, dataCallback, scope) {
                ref.child(id).remove();
                dataCallback();
                if (!scope.$$phase) scope.$apply();
            },
            updateNote: function (note, activeID) {
                note.id = activeID;
                var noteRef = ref.child(note.id);
                noteRef.update(angular.copy(note));
            },
            addIdea: function (id, idea) {
                var noteRef = ref.child(id);
                noteRef.child("ideas").push(JSON.parse(JSON.stringify(idea)));
            },
            removeIdea: function (noteID, ideaID) {
                var noteRef = ref.child(noteID);
                var ideaRef = noteRef.child("ideas").child(ideaID);
                ideaRef.remove();
            }
        }
    }])
    .factory('NoteContainer', ['NoteInterface', function NoteContainer(NoteInterface) {
        var NoteContainer = {};

        NoteContainer.test = function (str) {
            alert("Hi " + str);
        };

        NoteContainer.domains = [];
        NoteContainer.tags = [];
        NoteContainer.flat = {
            tags: [],
            domains: []
        };

        NoteContainer.authors = [
            new flagis.Author().add("Flo", "123456", "fgolemo@gmail.com", "#ff0"),
            new flagis.Author().add("Sven", "654321", "svelle@gmx.de", "#0ff"),
            new flagis.Author().add("Paolo", "abcdef", "paolo@aiai.com", "#f0f")
        ];

        /* BEGIN: for testing */
//        NoteContainer.notes = [
//            new flagis.Note()
//                .add("Erste Demonotiz", 0, NoteContainer.authors[0])
//                .addDomain("Psychology").addDomain("Biology")
//                .setID("12345"),
//            new flagis.Note()
//                .add("Totally cool idea", 2, NoteContainer.authors[0])
//                .addTag("tag1").addTag("tag2")
//                .addDomain("Philosophy")
//                .setID("65432"),
//            new flagis.Note()
//                .add("Senv has an insight", 1, NoteContainer.authors[1])
//                .addDomain("Psychology")
//                .addTag("tag1")
//                .setID("ABCDEF")
//        ];
        /* END: for testing */

//        NoteContainer.notes.$add(new flagis.Note()
//            .add("Erste Demonotiz", 0, NoteContainer.authors[0])
//            .addDomain("Psychology").addDomain("Biology")
//            .setID("12345"));
//
//        NoteContainer.notes.$add(new flagis.Note()
//            .add("Totally cool idea", 2, NoteContainer.authors[0])
//            .addTag("tag1").addTag("tag2")
//            .addDomain("Philosophy")
//            .setID("65432"));
//
//        NoteContainer.notes.$add(new flagis.Note()
//            .add("Senv has an insight", 1, NoteContainer.authors[1])
//            .addDomain("Psychology")
//            .addTag("tag1")
//            .setID("ABCDEF"));

        NoteContainer.everythingLabel = "-EVERYTHING-";

        NoteContainer.getProps = function (prop) {
            var distinctProps = [
                {name: NoteContainer.everythingLabel, count: NoteContainer.notes.length}
            ];
            for (var noteIndex in NoteContainer.notes) {
                for (var propIndex in NoteContainer.notes[noteIndex][prop]) {
                    var propVal = NoteContainer.notes[noteIndex][prop][propIndex];
                    if (propVal in distinctProps)
                        distinctProps[propVal].count++;
                    else
                        distinctProps[propVal] = {name: propVal, count: 1};

                }
            }
            var out = [];
            for (var domainIndex in distinctProps) {
                out.push(distinctProps[domainIndex]);
                if (domainIndex != 0 &&
                    NoteContainer.flat[prop].indexOf(distinctProps[domainIndex].name) == -1)
                    NoteContainer.flat[prop].push(distinctProps[domainIndex].name);
            }
            return out;
        }

        NoteContainer.filterType = "domains";
        NoteContainer.filter = '';
        NoteContainer.search = '';
        NoteContainer.active = new flagis.Note();
        NoteContainer.activeNoteID = -1;
        NoteContainer.getActiveNoteID = function () {
            return NoteContainer.activeNoteID
        };
        NoteContainer.setActiveNote = function (note) {
            NoteContainer.active = note
        };

        NoteContainer.loadNote = function (id) {
            NoteContainer.activeNoteID = id;
            if (id != -1)
                NoteContainer.active = $.grep(NoteContainer.notes, function (e) {
                    return e.id == id;
                })[0];
            else
                NoteContainer.active = new flagis.Note();
        }

        NoteContainer.ideaFactory = function () {
            return new flagis.Idea();
        }
        NoteContainer.commentFactory = function () {
            return new flagis.Comment();
        }
        NoteContainer.getIdeaClasses = function (mark) {
            var out = "glyphicon glyphicon-";
            if (mark == "0") // grounded
                return out + "ok green";
            else if (mark == "1") // inference
                return out + "share-alt blue";
            else if (mark == "2") // need research
                return out + "search orange";
            else // wild guess
                return out + "question-sign red";
        }

        NoteContainer.dataCallback = function () {
            //get Notes
            NoteContainer.domains = NoteContainer.getProps("domains");
            NoteContainer.tags = NoteContainer.getProps("tags");
        }

        NoteContainer.init = function (scope) {
            NoteContainer.notes = NoteInterface.getNotes(NoteContainer.dataCallback,
                scope, NoteContainer.getActiveNoteID, NoteContainer.setActiveNote);
        }

        return NoteContainer;
    }]).factory('Articles', ['$resource', function ($resource) {
        return $resource('api/query?keywords=:keywords', {}, {
            query: {method:'GET', params:{keywords:''}, isArray:true},
            get: {method:'GET', params:{keywords:''}, isArray:true},
        });

    }]);;

