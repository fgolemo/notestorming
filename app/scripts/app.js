'use strict';

angular.module('flagisv3App', [
        'ngResource',
        'ngRoute',
        'ngDebounce',
        'angularMoment',
        'ui.select2',
        'flagisv3App.directives',
        'flagisv3App.factories',
        'flagisv3App.filters',
        'flagisv3App.controllers'
        //'activeDomain', //loaded later
        //'array', //loaded later
    ])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/notes', {
                templateUrl: 'partials/notes',
                controller: 'NotesCtrl'
            })
            .when('/sources', {
                templateUrl: 'partials/sources',
                controller: 'SourcesCtrl'
            })
            .when('/consequences', {
                templateUrl: 'partials/consequences',
                controller: 'ConsequencesCtrl'
            })
            .otherwise({
                redirectTo: '/notes'
            });
        $locationProvider.html5Mode(true);
    }).run(['uiSelect2Config', function (uiSelect2Config) {
        uiSelect2Config.tokenSeparators = [",", " "];
        uiSelect2Config.multiple = true;
        uiSelect2Config.simple_tags = true;
    }]);