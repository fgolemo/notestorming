'use strict';

angular.module('flagisv3App.filters', []).filter(
    'activeDomain', ['NoteContainer', function (NoteContainer) {
        return function (input) {
            if (NoteContainer.filter == '') {
                return input;
            } else {
                var out = [];
                for (var noteIndex in input) {
                    var noteDomains = input[noteIndex][NoteContainer.filterType];
                    if (noteDomains && noteDomains.indexOf(NoteContainer.filter) !== -1)
                        out.push(input[noteIndex]);
                }
                return out;
            }
        };
    }]);

