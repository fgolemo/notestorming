'use strict';

var flagis = {};

flagis.Notes = function () {
    this.list = [];
};


flagis.Note = function () {
    this.id = "";
    this.name = "";
    this.ideas = [];
    this.importance = 0;
    this.domains = [];
    this.sources = [];
    this.datetime = new Date().getTime();
    this.comments = [];
    this.tags = [];
    this.relatedNotes = [];
    this.author = 0;
    this.deleted = false;
    this.consequences = [];
};
flagis.Note.prototype.add = function (name, importance, author) {
    this.name = name;
    this.importance = importance; // 0 = meh, 1 = kinda urgent, 2 = lives depend on it.
    this.author = author;
    return this;
};
flagis.Note.prototype.addIdea = function (content, mark, author) {
    var idea = new flagis.Idea().add(content, mark, author);
    this.ideas.push(idea);
    return this;
};
flagis.Note.prototype.remove = function (what, data) {
    var whatPlural = what + "s";
    var whatIndex = this[whatPlural].indexOf(data);
    if (whatIndex > -1) {
        this[whatPlural].splice(whatIndex, 1);
    }
    return this;
};
flagis.Note.prototype.addDomain = function (name) {
    if (this.domains.indexOf(name) === -1)
        this.domains.push(name);
    return this;
};
flagis.Note.prototype.addTag = function (name) {
    if (this.tags.indexOf(name) === -1)
        this.tags.push(name);
    return this;
};
flagis.Note.prototype.addComment = function (message, author) {
    var comment = new flagis.Comment().add(message, author);
    this.comments.push(comment);
    return this;
};
flagis.Note.prototype.setID = function (id) {
    this.id = id;
    return this;
};


flagis.Idea = function () {
    this.content = "";
    this.mark = 0; // 0 -
    this.author = 0;
};
flagis.Idea.prototype.add = function (content, mark, author) {
    this.content = content;
    this.mark = mark;
    this.author = author;
    return this;
};


flagis.Comment = function () {
    this.author = "";
    this.datetime = new Date().getTime();
    this.message = "";
};
flagis.Comment.prototype.add = function (message, author) {
    this.message = message;
    this.author = author;
    return this;
};


flagis.Author = function () {
    this.name = "";
    this.password = "";
    this.mail = "";
    this.color = "";
    this.notesSeen = [];
    this.idesSeen = [];
    this.commentsSeen = [];
    this.sourcesSeen = [];
    this.consequencesSeen = [];
};
flagis.Author.prototype.add = function (name, password, mail, color) {
    this.name = name;
    this.password = password;
    this.mail = mail;
    this.color = color;
    return this;
};


//flagis.Tag = function (name) {
//    this.name = name;
//};
//
//flagis.Domain = function (name) {
//    this.name = name;
//};

flagis.Consequence = function () {
    this.area = "";
    this.content = "";
    this.gradeImplentation = 0;
    this.author = "";
    this.tags = [];
    this.comments = [];
    this.notes = [];
    this.deleted = false;
};
//TODO: add
//TODO: attach to note and vice versa


flagis.Source = function () {
    this.name = "";
    this.notes = [];
    this.tags = [];
    this.domains = [];
    this.type = 0;
    this.info = "";
    this.comments = [];
    this.deleted = false;
    this.author = "";
};
//TODO: add by type
//TODO: attach to note and vice versa


flagis.ArgumentException = function (func) {
    this.name = "Argument Exception";
    this.level = "bad";
    this.func = func;
    this.message = "Error detected. Please proper arguments for the function: '" + this.func + "'";
    this.htmlMessage = this.message;
    this.toString = function () {
        return this.name + ": " + this.message
    };
};