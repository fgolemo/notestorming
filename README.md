# Intro

TODO

# Installation

+ Open a Unix shell, cd into any writable directory, make sure "git" is installed.
+ Clone this repository: `git clone git@bitbucket.org:fgolemo/notestorming.git`. This makes a new
directory: `notestorming`
+ So... `cd notestorming`
+ Make sure, NodeJS, NPM and Bower are installed. Node and NPM can be installed form the official
website or from certain repositories. And Bower is installed (globally) with
`sudo npm install -g bower`
+ Install all dependencies... first node modules, then bower stuff: `npm install && bower install`
+ Lean back, get a cup of tea... will take a while.
+ Once finished, you can start a local development webserver with `grunt serve`. That will also
launch your browser and point to the local server (so basically launching the app).
+ That's it.

# Development

"Livereload" is activated in grunt. That means, while `grunt serve` is running, if you change a file,
   grunt recognizes and reloads the webserver and the page in your browser.

When you made some changes to the code, every time you reach a semi-stable state, **commit**!
It is totally okay, to make new commits every half hour. It's like quicksave for your code.

+ add new and deleted files to Git versioning: `git add -A .`
+ commit the changes (i.e. save them locally... duh ^^): `git commit -v -a`

Once you made something cool, or you want to show off, **push** the stuff back to this repository:

+ (first make sure, nobody else made changes meanwhile): `git pull`
+ (If there are conflicts, solve them. If automatic merge happened, commit the merge):
   `git commit -v -a`
+ finally... actually push the stuff: `git push`

Now all participants get a mail, that you just pushed something. And we all can pull your changes
and merge it into our local dev environment.

# Deploying

The whole thing can (and occasionally will) be packed and deployed to a Heroku server at
[http://notestorming.herokuapp.com/](http://notestorming.herokuapp.com/).
If you have developer access to that app, you can deploy it on your own with the following commands:
(all in the project root folder)

+ make a grunt build in the "dist" folder: `grunt build`
+ if that fails... you mayyyy force it. But you shouldn't. Definitely: `grunt build --force`
+ if the build is successful, change into the dist folder `cd dist`, commit the changes and
push to Heroku:
    + `git add -A .`
    + `git commit -a -v`
    + `git push`
+ and voila, if you visit Heroku again, the app should represent the changes.
