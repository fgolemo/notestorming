'use strict';

var api = require('./controllers/api'),
    query = require('./controllers/query'),
    keywords = require('./controllers/keywords'),
    index = require('./controllers');

/**
 * Application routes
 */
module.exports = function (app) {

    // Server API Routes
    app.get('/api/awesomeThings', api.awesomeThings); //DEMO, can be deleted
    app.get('/api/query', query.getPapers);
    app.get('/api/testQuery', query.queryTest);
    app.get('/api/testUpdate', query.updateTest);
    app.get('/api/testWrappers', query.wrapperTest);
    app.get('/api/testSPARQL', query.sparqlTest);
    app.get('/api/query/:id', query.getRelated);
    app.get('/api/keywords', keywords.getList);


    // All undefined api routes should return a 404
    app.get('/api/*', function (req, res) {
        res.send(404);
    });

    // All other routes to use Angular routing in app/scripts/app.js
    app.get('/partials/*', index.partials);
    app.get('/*', index.index);
};