'use strict';

var config = require('../config/config');
var request = require("request");
var a = require('../entities/article');
var nssf = require('../services/nsStringFuncs');
var results = [];

//BEGIN private functions

var makeBaseSparql = function (keywords) {
    //console.log("got keywords for query: " + keywords);
    if (typeof keywords === 'string') {
        keywords = keywords.split(" ");
    }
    var query = [];
    query.push("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
    query.push("PREFIX saao: <" + config.onto + "> ");
    query.push("SELECT DISTINCT ?article ?title ?abstract ?link ?year WHERE { ");
    var queryBuffer = [];
    for (var i in keywords) {
//        queryBuffer.push("?article saao:hasTopic ?topic"+i+"");
//        queryBuffer.push("?topic"+i+" text:query (rdfs:label '" + keywords[i] + "')");
        queryBuffer.push("?article saao:hasTopic ?topic" + i);
        queryBuffer.push("?article saao:hasTitle ?title");
        queryBuffer.push("?article saao:hasURL ?link");
        queryBuffer.push("?article saao:publishedInYear ?year");
        queryBuffer.push("?topic" + i + ' rdfs:label ?label' + i);
        queryBuffer.push("OPTIONAL { ?article saao:hasAbstract ?abstract }" );
    }
    query.push(queryBuffer.join(" . ") + " .");
    queryBuffer = [];
    for (i in keywords) {
        queryBuffer.push('FILTER (?label' + i + ' = "' + keywords[i] + '")');
    }
    query.push(queryBuffer.join(" "));
    query.push("} LIMIT 10");
    //console.log("\n######## QUERY:\n" + query.join("\n"));
    return query.join("");
};

var queryTripleStore = function (keywords, next) {
    var query = makeBaseSparql(keywords);
    var endpoint = config.triStoreQuery;
//    console.log("Query to " + endpoint);
//    console.log("Query: " + query);
    var queryString = "?query=" + encodeURIComponent(query) + "&output=json";
    request(endpoint + queryString, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            var data = JSON.parse(body);
            if (!data || !data.results || !data.results.bindings) {
                console.log("Malformed response:");
                console.log(body);
                return next([]);
            }
            return next(data.results.bindings);
        } else {
            console.log("################# error during query:");
            console.log(error);
            return next([]);
        }
    });

};

var queryDataSources = function (keywords, next) {
    results = [];
    var answers = 0;

    var callback = function (data, ready) {
        results = results.concat(data);
        answers++;
        if (answers === config.wrappers.length) {
            next(results);
        }
    };

    for (var i in config.wrappers) {
        var wName = config.wrappers[i];
        var wrapper = require('../wrappers/' + wName);
        wrapper.query(keywords, callback);
    }
};

var getUpdateString = function (articles) {
    var out = [];
    out.push('PREFIX saao: <' + config.onto + '>');
    out.push('PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>');
    out.push('PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>');
    out.push('PREFIX owl: <http://www.w3.org/2002/07/owl#>');
    out.push('PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>');
    out.push('INSERT DATA {');
    for (var i in articles) { // is entity Article
        console.log("adding "+articles[i].id);
        out.push(articles[i].toTTL());
    }
    out.push('}');
    //console.log(out.join(" "));
    return out.join(" ");
};

var makeSPQRQLupdate = function (articles, next) {
    var updateString = getUpdateString(articles);
    request({
            method: 'POST',
            uri: config.triStoreUpdate,
            headers: {
                'Content-Type': 'application/sparql-update'
            },
            body: updateString

        }, function (error, response, body) {
            console.log(body);
            if (response.statusCode === 204) { //204 = no output
            } else {
                console.log('error: ' + response.statusCode);
//                console.log(body);
            }
            next(body);
        }
    );
};

//END private functions

//BEGIN public functions

/**
 * Main query rountine
 * @param req Request
 * @param res Response, list of papers
 */
exports.getPapers = function (req, res) {
    if (!req.query.keywords) {
        res.send(500, { error: 'I need some keywords to query for' });
        return;
    }
    console.log(req.query.keywords);

    queryTripleStore(req.query.keywords, function (queryDataA) {
        if (queryDataA.length < 10) {
            queryDataSources(req.query.keywords, function (newArticles) {
                makeSPQRQLupdate(newArticles, function (data) {
                    queryTripleStore(req.query.keywords, function (queryDataB) {
                        res.json(queryDataB);
                    });
                });
            });
        } else
            res.json(queryDataA);
    });
};

/**
 * Gets similar articles, based on the requested ID
 * @param req Request
 * @param res Response, list of papers
 */
exports.getRelated = function (req, res) {
    console.log("id:" + req.params.id);
    res.send(500, { error: 'not yet implemented' });
};

/**
 * Test the datascources
 * @param req Request, containing the keywords in req.query.keywords
 * @param res Response
 */
exports.wrapperTest = function (req, res) {
    if (!req.query.keywords) {
        res.send(500, { error: 'I need some keywords to query for' });
        return;
    }
    console.log(req.query.keywords);
    //TODO: check format of keywords

    queryDataSources(req.query.keywords, function (data) {
        res.json(data);
    });
};

/**
 * Test the sparql query builder
 * @param req Request, containing the keywords in req.query.keywords
 * @param res Response
 */
exports.sparqlTest = function (req, res) {
    if (!req.query.keywords) {
        res.send(500, { error: 'I need some keywords to query for' });
        return;
    }
    console.log(req.query.keywords);
    //TODO: check format of keywords

    var querystring = makeBaseSparql(req.query.keywords);
    res.send(querystring);
};

/**
 * Test the sparql query
 * @param req Request, containing the keywords in req.query.keywords
 * @param res Response
 */
exports.queryTest = function (req, res) {
    if (!req.query.keywords) {
        res.send(500, { error: 'I need some keywords to query for' });
        return;
    }
    console.log(req.query.keywords);

    queryTripleStore(req.query.keywords, function (data) {
        res.send(data);
    });

};

/**
 * Test the sparql update
 * @param req Request, containing the update string
 * @param res Response
 */
exports.updateTest = function (req, res) {
    var articles = [];
    var demoArticle = new a.Article();
    demoArticle.title = "Demo Article #1";
    demoArticle.id = "/&DEMO_.,:article";
    demoArticle.abstract = "/&DEMO_.,:article";
    demoArticle.link = "http://demoarticle.com";
    demoArticle.authors = ["Golemo, F", "Hooplot, M", "van Leipsig, S"];
    demoArticle.keywords = ["demo", "article", nssf.camelCase("linked data")];
    demoArticle.pubDate = "2014-03-25";
    articles.push(demoArticle);

    makeSPQRQLupdate(articles, function (data) {
        res.send(data);
    });

};

//END public functions
