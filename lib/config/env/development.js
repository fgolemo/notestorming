'use strict';

module.exports = {
    env: 'development',
    wrappers: ["scopus_wrapper", "sciencedirect_wrapper", "springer_wrapper"]
};