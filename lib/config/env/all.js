'use strict';

var path = require('path');

var rootPath = path.normalize(__dirname + '/../../..');

module.exports = {
    root: rootPath,
    port: process.env.PORT || 3000,
    onto: "http://www.semanticweb.org/gust/ontologies/2014/2/simple-articel-author-onto#",
    triStoreQuery: "http://notestorming-fuseki.herokuapp.com/inf/sparql",
    triStoreUpdate: "http://notestorming-fuseki.herokuapp.com/inf/update",
    wrappers: ["scopus_wrapper", "sciencedirect_wrapper", "springer_wrapper"]
};