'use strict';

var xregexp = require('xregexp');

exports.nonCapMerge = function (input) {
    return (input.charAt(0).toLowerCase() + input.substring(1)).replace(/ /g, "");
}

/**
 * camelCases a string
 * @param str Input String
 */
exports.camelCase = function (input) {
    return input.toLowerCase().replace(/ (.)/g, function (match, group1) {
        return group1.toUpperCase();
    });
};

exports.camelCaseKeywords = function (input, fromUser) {
    var keywordsTmp = input.split(" ");
    var keywords = [];
    for (var i in keywordsTmp) {
        if (fromUser)
            keywords.push(exports.nonCapMerge(keywordsTmp[i]));
        else
            keywords.push(exports.camelCase(keywordsTmp[i]));
    }
    return exports.arrayUnique(keywords);
}

exports.getLastNameRE = function () {
    return xregexp.XRegExp('(\\p{L}*[. -]*\\p{L}*[. -]*\\p{L}*[. -]*\\p{L}*[. -]*\\p{L}+), ?((\\p{L}+)\\.?[- ]?)((\\p{L}+)\\.?[-]?)?((\\p{L}+)\\.?[- ]?)?');
}

exports.getFirstNameRE = function () {
    return xregexp.XRegExp('((\\p{L}+)\\.?[- ]?)((\\p{L}+)\\.?[- ]?)?((\\p{L}+)\\.?[- ]?)?, ?(\\p{L}*[. -]*\\p{L}*[.-]*\\p{L}*[. -]*\\p{L}*[. -]*\\p{L}+)');
}

/**
 * Filters an array, keeps only unique values
 * @param array
 * @returns array
 */
exports.arrayUnique = function (a) {
    return a.reduce(function (p, c) {
        if (p.indexOf(c) < 0) p.push(c);
        return p;
    }, []);
};

exports.makeName = function (last, first) {
    return "Person-"+(last+"-"+first).replace(/ /g, "-");
};

exports.makeID = function (input) {
    return "Article-" + input.replace(/[^A-Za-z0-9-]/g, "-");
};

exports.makeTopic = function (input) {
    return "Topic-" + input.replace(/[^A-Za-z0-9-]/g, "-");
};

