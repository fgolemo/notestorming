'use strict';

var config = require('../config/config');
var nssf = require('../services/nsStringFuncs');

exports.Article = function () {
    return {
        title: "",
        abstract: "",
        link: "",
        authors: [],
        keywords: [],
        pubDate: "",
        id: "",
        wrapper: "",
        toTTL: function () {
            var out = [];
            var authors = [];
            var topics = [];
            out.push("saao:" + nssf.makeID(this.id) + " a saao:Article");
            out.push('saao:hasTitle "' + this.title.replace(/["\\]/g,"") + '"');
            if (this.abstract)
                out.push('saao:hasAbstract "' + this.abstract.replace(/["\\]/g,"") + '"');
            if (this.link)
                out.push('saao:hasURL "' + this.link + '"');
            out.push('saao:publishedInYear ' + this.pubDate.substring(0, 4));
            for (var i in this.authors) {
                var authorSplit = this.authors[i].split(", ");
                var authorID = nssf.makeName(authorSplit[0], authorSplit[1]);
                if (this.authors.length == 1)
                    out.push('saao:hasCreator saao:' + authorID);
                else
                    out.push('saao:hasAuthor saao:' + authorID);
                var authorLine = [];
                authorLine.push('saao:' + authorID + ' a saao:Author');
                authorLine.push('saao:hasFirstName "' + authorSplit[1] + '"');
                authorLine.push('saao:hasLastName "' + authorSplit[0] + '"');
                authors.push(authorLine.join(" ; ") + " . ");
            }
            for (var i in this.keywords) {
                var topicID = nssf.makeTopic(this.keywords[i]);
                out.push('saao:hasTopic saao:' + topicID);
                var topicLine = [];
                topicLine.push('saao:' + topicID + ' a saao:Topic');
                topicLine.push('rdfs:label "' + this.keywords[i] + '"');
                topics.push(topicLine.join(" ; ") + " . ");
            }
            return out.join(" ; ") + " . " + authors.join(" ") + topics.join(" ");
        }
    };
};