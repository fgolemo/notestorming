'use strict';
var request = require("request");

var a = require('../entities/article');
var nssf = require('../services/nsStringFuncs');
var puny = require('punycode');

exports.query = function (keyword, next) {
    var url = "http://api.elsevier.com/content/search/index:SCOPUS?apiKey=4746936e0d341c1f48c533838b9f6d37&query=" + String(keyword) + "&count=10&sort=+relevancy&httpAccept=application%2Fjson";
    var keywords = nssf.camelCaseKeywords(keyword, true);
    var outputList = [];
    request(url, function (error, response, body) {
        if (error || response.statusCode !== 200) {
            console.log(error);
            next([]);
            return;
        }
        var resp = JSON.parse(body);
        for (var paper in resp["search-results"].entry) {
            var tmpPaper = new a.Article();
            tmpPaper.wrapper = "scopus";
            tmpPaper.id = resp["search-results"].entry[paper]["dc:identifier"];
            tmpPaper.title = resp["search-results"].entry[paper]["dc:title"];
            if (typeof (resp["search-results"].entry[paper]["dc:creator"]) !== 'undefined') {
                var tmpauthor = (resp["search-results"].entry[paper]["dc:creator"]).split("|");
                for (var author in tmpauthor) {
                    var re = nssf.getLastNameRE();
                    var nameString = String(tmpauthor[author]);
                    var match = nameString.match(re);
                    if (!match)
                        console.log("name doesn't match: " + nameString);
                    else {
                        var lastName = match[1];
                        var firstNames = [];
                        firstNames.push(match[3].charAt(0));
                        if (match[5]) {
                            firstNames.push(match[5].charAt(0));
                        }
                        if (match[7]) {
                            firstNames.push(match[7].charAt(0));
                        }
                        var firstName = firstNames.join(" ");
                        tmpPaper.authors.push(lastName + ", " + firstName);
                    }
                }
            }
            tmpPaper.abstract = resp["search-results"].entry[paper]["prism:teaser"]; // is teaser not actual abstract
            if (resp["search-results"].entry[paper].link) {
                tmpPaper.link = resp["search-results"].entry[paper].link[1]["@href"];
            }
            tmpPaper.pubDate = resp["search-results"].entry[paper]["prism:coverDate"];
            //tmpPaper.citeCount = resp["search-results"].entry[paper]["citedby-count"];

            tmpPaper.keywords = keywords;
            tmpPaper.ttl = tmpPaper.toTTL(); // TTL serialisation test
            outputList.push(tmpPaper);
        }
        next(outputList);
    });
};
