'use strict';

var a = require('../entities/article');

/**
 * Get a list of available keywords
 * @param keywords Array with all the query keywords as strings
 */
exports.query = function (keywords, next) {

    //TODO: execute query to source, then put each result into an "Article" object
    //general idea:

//    var output = [];
//    //this next method doesnt exist. But sthg. similar should be there in the "http"
//    // or better "request" module
//    http.get("http://www.springerlink.com/query=" + keywords).success(function(data) {
//        data.forEach(function (item) {
//            var article = new a.Article();
//            article.title = item["dc:title"];
//            article.abstract = ...
//            output.push(article);
//        });
//        next(output);
//    });



    //make a demo article from the Article class
    var testArticle1 = new a.Article();

    //fill the demo article with data
    testArticle1.title = "mind-blowing article #1";
    testArticle1.abstract = "cool long abstract";
    testArticle1.keywords = ["science", "cool"];
    testArticle1.authors = [
        ["Sam", "van Leipsig"],
        ["Florian", "Golemo"],
        ["J", "Without Firstname"]
    ];

    //another demo article
    var testArticle2 = new a.Article();
    testArticle2.title = "sciency article #2";
    testArticle2.abstract = "informative abstract text";
    testArticle2.keywords = ["yeah", "test", "what am I doing here"];
    testArticle2.authors = [
        ["Schni", "Tzel"]
    ];

    //new empty output list (to hold the Article objects)
    var output = [];

    //now add the objects to the list
    output.push(testArticle1);
    output.push(testArticle2);

    //send the stuff back to the caller
    next(output); //call this when finished
};
