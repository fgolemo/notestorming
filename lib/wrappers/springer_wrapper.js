'use strict';
var request = require("request");

var a = require('../entities/article');
var nssf = require('../services/nsStringFuncs');
var puny = require('punycode');

exports.query = function(keyword, next) {
    var url = "http://api.springer.com/metadata/json?q=" + String(keyword) + "&p=10&type:Journal&api_key=kykynczj4zdewqdj33yjufpz";
    var outputList = [];
    request(url, function(error, response, body) {
        if (error || response.statusCode !== 200) {
            console.log(error);
            next([]);
            return;
        }
        var resp = JSON.parse(body);
        var tempkeywords = [];
        for (var value in resp.facets[1].values) {
            if (value < 6) {
                tempkeywords.push(nssf.camelCase(resp.facets[1].values[value].value));
            }
        }
        for (var paper in resp.records) {
            var tmpPaper = new a.Article();
            tmpPaper.wrapper = "springer";
            tmpPaper.id = resp.records[paper].identifier;
            tmpPaper.title = resp.records[paper].title;
            for (var creator in resp.records[paper].creators) {
                var re = nssf.getLastNameRE();
                var nameString = String(resp.records[paper].creators[creator].creator);
                var match = nameString.match(re);
                if (!match)
                    console.log("name doesn't match: " + nameString);
                else {
                    var lastName = match[1];
                    var firstNames = [];
                    firstNames.push(match[3].charAt(0));
                    if (match[5]) {
                        firstNames.push(match[5].charAt(0));
                    }
                    if (match[7]) {
                        firstNames.push(match[7].charAt(0));
                    }
                    var firstName = firstNames.join(" ");
                    tmpPaper.authors.push(lastName + ", " + firstName);
                }
            }
            tmpPaper.abstract = resp.records[paper].abstract;
            tmpPaper.link = resp.records[paper].url;
            tmpPaper.pubDate = resp.records[paper].publicationDate;
            tmpPaper.keywords = nssf.arrayUnique(tempkeywords);
            tmpPaper.ttl = tmpPaper.toTTL(); // TTL serialisation test
            outputList.push(tmpPaper);
        }
        next(outputList);
    });
};